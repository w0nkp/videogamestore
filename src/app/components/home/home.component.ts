import { Component, OnInit } from '@angular/core';
import { VideoGameServiceService } from 'src/app/video-games/services/video-game-service.service';
import { VideoGame } from 'src/app/models/video-game';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  public videoGames: VideoGame[];


  constructor(private videoGameService: VideoGameServiceService) { }

  ngOnInit() {
    this.videoGames = this.videoGameService.getVideoGames();
  }


}

