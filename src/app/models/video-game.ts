export class VideoGame {
    name: string;
    description: string;
    price: number;
    imageUrl: string;
    id: number;
}