import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VideoGameServiceService } from '../services/video-game-service.service';
import { VideoGame } from 'src/app/models/video-game';
import * as $ from 'jquery';



@Component({
  selector: 'app-submit-game',
  templateUrl: './submit-game.component.html',
  styleUrls: ['./submit-game.component.scss']

})
export class SubmitGameComponent implements OnInit {

  myForm: FormGroup;
  public success = false;

  constructor(private fb: FormBuilder, private videoGameService: VideoGameServiceService) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      game: ['', [Validators.required, Validators.minLength(2), Validators.min(18), Validators.max(65)]],
      price: [null, [Validators.required, Validators.min(1)]],
      description: ['', [Validators.required, Validators.maxLength(280)]],
      imageUrl: ['', [Validators.required]] 
  
    })

    this.myForm.valueChanges.subscribe(console.log)
  }

  get game() {
    return this.myForm.get('game');
  }

  get price() {
    return this.myForm.get('price');
  }

  get description() {
    return this.myForm.get('description');
  }

  get imageUrl() {
    return this.myForm.get('imageUrl');
  }

  submitHandler() {


    var formValue = this.myForm;
    if (formValue.valid)  
      { 
        var _test = this.videoGameService.addVideoGame(formValue.value);
        if(_test === -1){

         // $('.toast').toast('show');
          document.getElementsByClassName('toast');
        }
      }
      
  }
}
