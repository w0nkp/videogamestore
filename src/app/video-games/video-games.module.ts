import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewGameComponent } from './view-game/view-game.component';





@NgModule({
    declarations: [ViewGameComponent],
    exports: [
        ViewGameComponent
    ],
    imports: [
        CommonModule
    ]
})
export class VideoGamesModule { }
