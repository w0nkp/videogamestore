import { Injectable } from '@angular/core';
import { VideoGame } from 'src/app/models/video-game';


@Injectable({
  providedIn: 'root'
})
export class VideoGameServiceService {

  private videoGames: VideoGame[];
  constructor() {
    this.videoGames = [{ name: 'Snake', description: 'Snake? Snake!?!? Snaaaaaaaakeee!!!', price: 10, id: 1, imageUrl: 'https://www.mobygames.com/images/covers/l/387841-snake-pass-playstation-4-front-cover.png' },
                       { name: 'Super Meat Boy', description: 'Your run of the mill 90\'s nostalgia', price: 10, id: 2, imageUrl: 'https://www.mobygames.com/images/covers/l/241778-super-meat-boy-ultra-rare-edition-windows-front-cover.jpg' },
                       { name: 'Heave Ho', description: 'Hold on to dear life', price: 10, id: 3, imageUrl: 'https://www.mobygames.com/images/covers/l/584898-heave-ho-nintendo-switch-front-cover.jpg' },
                       { name: 'Sponge Bob: Super Pants', description: 'He\'s Ready', price: 10, id: 4, imageUrl: 'https://www.mobygames.com/images/covers/l/238225-spongebob-squarepants-supersponge-playstation-front-cover.jpg' },
                       { name: 'Spelunky', description: 'Prove your Cave expertise', price: 10, id: 5, imageUrl: 'https://www.mobygames.com/images/covers/l/274756-spelunky-playstation-3-front-cover.jpg' },
                       
                      ]
  }

  getVideoGames() {
    return this.videoGames;
  }

  findById(id:number) {
    return this.videoGames.find(item => item.id == id);
  }

  addVideoGame(videoGame) {
    try{
    var newVideoGame = new VideoGame(); 
    newVideoGame.name = videoGame.name;
    newVideoGame.description =videoGame.description;
    newVideoGame.price = videoGame.price;
    newVideoGame.id = videoGame.id;
    newVideoGame.imageUrl = videoGame.imageUrl;

    this.videoGames.push(newVideoGame);
    //throw new Error('Something bad happened');
    return -1;
    }
    catch(e){
      alert("aa");
    }
    
  }
}

