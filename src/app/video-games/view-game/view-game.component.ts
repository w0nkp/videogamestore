import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VideoGameServiceService } from '../services/video-game-service.service';
import { VideoGame } from 'src/app/models/video-game';


  

@Component({
  selector: 'app-view-game',
  templateUrl: './view-game.component.html',
  styleUrls: ['./view-game.component.scss']
})
export class ViewGameComponent implements OnInit {


  public videoGame: VideoGame;
  constructor(private route: ActivatedRoute, private videoGameService: VideoGameServiceService) { 

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      
      this.videoGame = this.videoGameService.findById(params['id']);
    });
  }

}
