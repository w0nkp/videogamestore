import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { GamesComponent } from './components/games/games.component';
import { ViewGameComponent } from './video-games/view-game/view-game.component';
import { SubmitGameComponent } from './video-games/submit-game/submit-game.component';






const routes: Routes = [{ path: '', component: HomeComponent },
{ path: 'games', component: GamesComponent },
{ path: 'video-games/view-game/:id', component: ViewGameComponent },
{ path: 'video-games/submit-game', component: SubmitGameComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
