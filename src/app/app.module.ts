import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeDirective } from './home.directive';
import { HomeComponent } from './components/home/home.component';
import { VideoGamesModule } from './video-games/video-games.module';
import { GamesComponent } from './components/games/games.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SubmitGameComponent } from './video-games/submit-game/submit-game.component';

import * as $ from 'jquery';





@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HomeDirective,
    GamesComponent,
    SubmitGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    VideoGamesModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    CommonModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
